#ifndef PIDTestingTool_PIDAnalysis_H
#define PIDTestingTool_PIDAnalysis_H

#include "TRTFramework/TRTAnalysis.h"
#include "PIDTestingTool/Likelihood2.h"
#include "PIDTestingTool/Likelihood.h"

class PIDAnalysis : public TRTAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  Likelihood2 *m_Likelihood2; //!
  Likelihood *m_Likelihood; //!
 
  // common strings
  TString gasTypes[3] = { "_Xe",  "_Ar",  ""  };
  TString TRTparts[3] = { "_BRL", "_ECA", "_ECB" };

public:
  // this is a standard constructor
  PIDAnalysis();
  virtual ~PIDAnalysis();

  // these are the functions inherited from TRTAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode initialize();

  void loopOverHits( const xAOD::TrackParticle *track, bool isElectron );
  bool passZSelection( xAOD::IParticleContainer lep );
  
  inline virtual Likelihood2* LH2() { return m_Likelihood2; }
  inline virtual Likelihood* LH() { return m_Likelihood; }


  // this is needed to distribute the algorithm to the workers
  ClassDef(PIDAnalysis, 1);
};

#endif // PIDTestingTool_PIDAnalysis_H
