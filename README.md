Author: jared.vasquez@yale.edu

Please feel free to report any bugs, suggestions, missing features or praise to the author.

This testing script is built on the basis of the [TRT Framework](https://gitlab.cern.ch/jvasquez/TRTFramework).

Getting Started
======================

Create a Workspace
--------------------
```
cd ~/
mkdir -p trt_workspace/packages trt_workspace/run
````

Setup the current Analysis Release
-------------------------------------
For the first time, setup the release you're working with:
```
cd trt_workspace/packages
setupATLAS
rcSetup Base,2.4.3
```

To setup the same version again:
```
setupATLAS
rcSetup
```

Setup the code to compile with the release used above
---------------------------------------------------------
For the first time checkout a copy of the TRTFramework and PIDTestingTool, then compile.
```
git clone ssh://git@gitlab.cern.ch:7999/jvasquez/TRTFramework.git
git clone ssh://git@gitlab.cern.ch:7999/jvasquez/PIDTestingTool.git
rc find_packages; rc compile
```

Future updates can be pulled from the repository and compiled like so:
```
git pull origin master
rc find_packages; rc compile
```

Running your code
======================
It is recommended that your analysis code is always ran from the ``run/`` directroy.
```
cd ../run
```

Running the code is as simple as calling ``runPIDAnalysis`` and then specifying a config file.
For example: 

```
runPIDAnalysis PIDTestingTool/PID_Zee.cfg 
```

There are many more options available however
```
    runMyAnalysis [CONFIG-FILES] [root files] [KEY: VALUE]

    [CONFIG-FILES] TEnv text files with settings used both by algortihm and job submission.
    [root files] if argument is of the form *.root* these will be used as input
    Some basic config KEYs, which follow the same format as the CONFIG-FILE, are:
      InputFile:     specifies an input ROOT file. Can be used multiple times
      InputFileList: specifies a text file containing a list of ROOT files (with full PATH) to run over
      GridDS:        specifies a grid data sample to run over
      OutputDir:     specifies ouput directory. DATE is replaced by date+time [default: runPIDAnalysis_DATE]
      SampleName:    specifies sample name [default: sample]
      BaseConfig:    overrides the default base configuration file (calibration smearing etc)
```

Your analysis configuration files should all inherit from a ``BaseConfig`` file which 
specifies all the default configurations to be used. Your analysis config file however
can be used to overwrite any of these options. 

An example is provided [here](https://gitlab.cern.ch/jvasquez/PIDTestingTool/blob/master/data/PID_Zee.cfg).


Running your code on the GRID
--------------------------------
Setup environment

```
cd ~/trt_workspace/packages
setupATLAS
localSetupFAX 
rcSetup 
localSetupPandaClient --noAthenaCheck
voms-proxy-init -voms atlas 
```

Run the script from the ``run/`` directory (important)

```
cd ../run 
runPIDAnalysis YourAnalysisConfig.cfg GridDS: XXX OutputDS: user.USERNAME.XXX SampleName: XXX 
```

Be patient as the sripts run, submitting the jobs to the grid will take some time. 
For the jobs to copy to the grid correctly it is important that the directory you 
submit your jobs from is not a subdirectory of packages.