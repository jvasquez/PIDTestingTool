#include "PIDTestingTool/PIDAnalysis.h"
#include "TRTFramework/RunUtils.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  PIDAnalysis *alg = new PIDAnalysis();

  // Use helper to start the job
  TRT::runJob(alg, argc, argv);

  return 0;
}
