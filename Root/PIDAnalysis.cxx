#include "PIDTestingTool/PIDAnalysis.h"
#include "PIDTestingTool/PIDHelpers.h"
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(PIDAnalysis)



PIDAnalysis::PIDAnalysis()
: TRTAnalysis()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



PIDAnalysis::~PIDAnalysis()
{
  // Here you delete any memory you allocated during your analysis.
}


EL::StatusCode PIDAnalysis::initialize()
{
  TRTAnalysis::initialize();  // Must Keep This Line

  m_Likelihood = new Likelihood();

  m_Likelihood2 = new Likelihood2();
  m_Likelihood2->initialize( *config() );
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  TRTAnalysis::execute();  // Must Keep This Line

  // Remove Duplicate Events
  if ( isDuplicate() ) return EL::StatusCode::SUCCESS;

  // Require GRL
  if ( not passGRL() ) return EL::StatusCode::SUCCESS;

  // Remove Events with Zero Weight
  if ( hasZeroWeight() ) return EL::StatusCode::SUCCESS;

  // Get Lepton Containers
  xAOD::ElectronContainer electrons = lepHandler()->getSelectedElectrons();
  xAOD::MuonContainer         muons = lepHandler()->getSelectedMuons();
  if ( electrons.size() == 0 && muons.size() == 0 ) return EL::StatusCode::SUCCESS;

  // Decide if electron or muon selection....
  bool useElectrons(false), useMuons(false);
  TString sampleName = config()->getStr("SampleName"); 

  if      ( sampleName.Contains("Zee") ) useElectrons = true; 
  else if (sampleName.Contains("Zmumu")) useMuons = true; 
  else TRT::fatal( "Must select supported selection [ Zee, Zmumu ]" );

  xAOD::IParticleContainer leptons;
  if (useMuons) leptons = muons;
  else          leptons = electrons;

  // Require events pass dilepton Z selection
  if ( not passZSelection( leptons ) ) return EL::StatusCode::SUCCESS;
 
  // -----------------------------------------------------------------------------

  // Set Event Weight
  double w = (isData()) ? 1.0 : weight();
  
  // Fill Lepton Kinematic Plots 
  histoStore()->fillTH1F( "h_massZll",   TRT::getDilepMass(leptons), w );
  histoStore()->fillTH1F(  "h_lep_pt", leptons[0]->pt()*TRT::invGeV, w );
  histoStore()->fillTH1F(  "h_lep_pt", leptons[1]->pt()*TRT::invGeV, w );
  histoStore()->fillTH1F( "h_lep_pt1", leptons[0]->pt()*TRT::invGeV, w );
  histoStore()->fillTH1F( "h_lep_pt2", leptons[1]->pt()*TRT::invGeV, w );
  
  // Pileup Histograms
  histoStore()->fillTH1F( "h_avgMu",  averageMu(), w);
  histoStore()->fillTH1F( "h_NPV",    numberOfPrimaryVertices(), w );
 
  // Occupancy Histograms 
  histoStore()->fillTH1F( "h_OccGlobal",    eventInfo()->auxdata<float>("TRTOccGlobal"),   w ); 
  histoStore()->fillTH1F( "h_OccBarrelA",   eventInfo()->auxdata<float>("TRTOccBarrelA"),  w ); 
  histoStore()->fillTH1F( "h_OccBarrelC",   eventInfo()->auxdata<float>("TRTOccBarrelC"),  w ); 
  histoStore()->fillTH1F( "h_OccEndcapAA",  eventInfo()->auxdata<float>("TRTOccEndcapAA"), w ); 
  histoStore()->fillTH1F( "h_OccEndcapAC",  eventInfo()->auxdata<float>("TRTOccEndcapAC"), w ); 
  histoStore()->fillTH1F( "h_OccEndcapBA",  eventInfo()->auxdata<float>("TRTOccEndcapBA"), w ); 
  histoStore()->fillTH1F( "h_OccEndcapBC",  eventInfo()->auxdata<float>("TRTOccEndcapBC"), w ); 
 
  int iTrack(0);
  // Loop Over Electron
  if ( useElectrons ) { 
    for( auto electron: electrons ) {
      iTrack++; if ( iTrack > 2 ) continue;
      const xAOD::TrackParticle* track = lepHandler()->getTrack( electron );
      if ( not lepHandler()->passSelection( track ) ) continue;
      histoStore()->fillTH1F( "h_trk_pt",  track->pt()*TRT::invGeV, w );
      if ( iTrack == 1 ) histoStore()->fillTH1F( "h_trk_pt1", track->pt()*TRT::invGeV, w );
      if ( iTrack == 2 ) histoStore()->fillTH1F( "h_trk_pt2", track->pt()*TRT::invGeV, w );
      loopOverHits( track, true );
    }

  // Loop Over Muons 
  } else if ( useMuons ) {
    for( auto muon: muons ) {
      iTrack++; if ( iTrack > 2 ) continue;
      const xAOD::TrackParticle* track = lepHandler()->getTrack( muon );
      if ( not lepHandler()->passSelection( track ) ) continue;
      histoStore()->fillTH1F( "h_trk_pt",  track->pt()*TRT::invGeV, w );
      if ( iTrack == 1 ) histoStore()->fillTH1F( "h_trk_pt1", track->pt()*TRT::invGeV, w );
      if ( iTrack == 2 ) histoStore()->fillTH1F( "h_trk_pt2", track->pt()*TRT::invGeV, w );
      loopOverHits( track, false );
    }
  }

  return EL::StatusCode::SUCCESS;
}

