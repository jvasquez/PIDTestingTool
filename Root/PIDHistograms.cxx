#include "PIDTestingTool/PIDAnalysis.h"
#include "TRTFramework/TRTIncludes.h"


EL::StatusCode PIDAnalysis::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.
 
  // Lepton Kinematics 
  histoStore()->createTH1F( "h_massZll", 50, 70., 120., "; #it{m}_{ll} [GeV]; Events");
  histoStore()->createTH1F(  "h_lep_pt", 60,  0., 120., "; #it{p}_{T}^{lepton} [GeV]; Events");
  histoStore()->createTH1F( "h_lep_pt1", 60,  0., 120., "; #it{p}_{T}^{lepton, lead} [GeV]; Events");
  histoStore()->createTH1F( "h_lep_pt2", 60,  0., 120., "; #it{p}_{T}^{lepton, subl} [GeV]; Events");
  
  // Track Kinematics
  histoStore()->createTH1F( "h_trk_pt",  60,  0., 120., "; #it{p}_{T}^{track} [GeV]; Events");
  histoStore()->createTH1F( "h_trk_pt1", 60,  0., 120., "; #it{p}_{T}^{track, lead} [GeV]; Events");
  histoStore()->createTH1F( "h_trk_pt2", 60,  0., 120., "; #it{p}_{T}^{track, subl} [GeV]; Events");
  
  // Pileup Histograms
  histoStore()->createTH1F(      "h_NPV",  30,  0,  30, "; N_{PV}; Events");
  histoStore()->createTH1F(    "h_avgMu",  30,  0,  30, "; #LT#mu#GT; Events");
  histoStore()->createTH1F( "h_avgMu_OT",  30,  0,  30, "; #LT#mu#GT; Events");
 
  // Occupancy Histograms 
  histoStore()->createTH1F( "h_OccTrack",    66, -0.005, 0.655, "; TRT Track Occupancy; Events" );
  histoStore()->createTH1F( "h_OccGlobal",   66, -0.005, 0.655, "; TRT Global Occupancy; Events" );
  histoStore()->createTH1F( "h_OccBarrelA",  66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  histoStore()->createTH1F( "h_OccBarrelC",  66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  histoStore()->createTH1F( "h_OccEndcapAA", 66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  histoStore()->createTH1F( "h_OccEndcapAC", 66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  histoStore()->createTH1F( "h_OccEndcapBA", 66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  histoStore()->createTH1F( "h_OccEndcapBC", 66, -0.005, 0.655, "; TRT Regional Occupancy; Events" );
  
  // Count HT Hits
  histoStore()->createTH1F(      "h_nHT", 40, -0.5, 39.5, "; n_{HT}; Events / Bin" );
  histoStore()->createTH1F(    "h_nHTMB", 40, -0.5, 39.5, "; n_{HTMB}; Events / Bin" );
  histoStore()->createTH1F(   "h_nHT_Ar", 40, -0.5, 39.5, "; n^{Ar}_{HT}; Events / Bin" );
  histoStore()->createTH1F( "h_nHTMB_Ar", 40, -0.5, 39.5, "; n^{Ar}_{HTMB}; Events / Bin" );
  
  // -------------------------
  //  HT Probability Plots 
  // -------------------------
 
  // Define binning and ranges for profiles
  int NbinsSL[3] = {73, 96, 64};
  int NbinsZR[3] = {36, 40, 40};
  double ZRmin[3] = {  0.0,  630.0,  630.0};
  double ZRmax[3] = {720.0, 1030.0, 1030.0};
  int NbinsTW = 44; double TWmin = 0.0; double TWmax = 2.2;
   
  // Divide gamma range into bins logarithmically:
  double gMin = 10.0;
  const int NgBins(50), NgBinDeca(10); 
  std::vector<double> gBins;
  for (int ig=0; ig < NgBins+3; ig++) 
    gBins.push_back( gMin * pow(10.0, double(ig)/double(NgBinDeca)) );
  
  // create TProfiles for HT fraction plots.
  TString hname, suffix;
  for( int ig(0); ig < 3; ig++ ) {
    for( int ip(0); ip < 3; ip++ ) {

      suffix = gasTypes[ig] + TRTparts[ip];
      hname = "h_pHT" + suffix;
      histoStore()->createTProfile( hname+"_AVG", 1, -5., 5., "; Arbitrary Unit; HT Probability" );
      histoStore()->createTProfile( hname+ "_GF", gBins, "; #gamma-Factor; HT Probability" );
      histoStore()->createTProfile( hname+ "_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; Straw Layer; HT Probability" ); 
      histoStore()->createTProfile( hname+ "_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z/R Position [mm]; HT Probability" ); 
      histoStore()->createTProfile( hname+ "_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire Distance [mm]; HT Probability" ); 
      histoStore()->createTProfile( hname+ "_OT", 66, -0.005, 0.655, "; TRT Track Occupancy; HT Probability" );
     
      hname = "h_pHTMB" + suffix;
      histoStore()->createTProfile( hname+"_AVG", 1, -5., 5., "; Arbitrary Unit; HT_{MB} Probability" );
      histoStore()->createTProfile( hname+ "_GF", gBins, "; #gamma-Factor; HT_{MB} Probability" );
      histoStore()->createTProfile( hname+ "_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; Straw Layer; HT_{MB} Probability" ); 
      histoStore()->createTProfile( hname+ "_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z/R Position [mm]; HT_{MB} Probability" ); 
      histoStore()->createTProfile( hname+ "_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire Distance [mm]; HT_{MB} Probability" ); 
      histoStore()->createTProfile( hname+ "_OT", 66, -0.005, 0.655, "; TRT Track Occupancy; HT_{MB} Probability" );

      // 2D Onset Histograms
      histoStore()->createTH2F(   "h2_nLT"+suffix+"_GF_OT", gBins, 50, 0, 1, "; #gamma-Factor; TRT Track Occupancy; Events" );
      histoStore()->createTH2F(   "h2_nHT"+suffix+"_GF_OT", gBins, 50, 0, 1, "; #gamma-Factor; TRT Track Occupancy; Events" );
      histoStore()->createTH2F( "h2_nHTMB"+suffix+"_GF_OT", gBins, 50, 0, 1, "; #gamma-Factor; TRT Track Occupancy; Events" );

    }
  }
  
  // Electron Probability Profiles
  for ( int iLH(0); iLH < 8; iLH++ ) {
    hname = TString::Format( "h3_eProb%d_OT", iLH );
    histoStore()->createTH3F( hname, 5, -0.5, 4.5, 200, 0.0, 1.0, 50, 0.0, 1.0, "; etaBin; electron Probability; Track Occupancy");
  }


  return EL::StatusCode::SUCCESS;
}
