////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }

TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn) 
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access 2D histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *getSliceX( TH2 *h2, int ymin=0, int ymax=-1 )
{
  int NbinsX = h2->GetNbinsX();
  if (ymax < 0) ymax = h2->GetNbinsY()+1;
  
  TH1 *h = (TH1*)h2->ProjectionX( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int xbin(0); xbin <= NbinsX+1; xbin++ ) {
    int nvals(0);
    double avg(0), err(0);
    for( int ybin = ymin; ybin <= ymax; ybin++ ) { 
      double val = h2->GetBinContent( xbin, ybin ); 
      double dx  = h2->GetBinError( xbin, ybin );
      if( val <= 0 ) continue;
      nvals++;
      avg += val;
      err += (dx*dx);
    }
    if (nvals <= 0) continue;
    avg /= nvals;
    err = sqrt(err)/nvals;
    h->SetBinContent(xbin, avg);
    h->SetBinError(xbin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  return h;
}

TH1 *getSliceY( TH2 *h2, int xmin=0, int xmax=-1 )
{
  int NbinsY = h2->GetNbinsY();
  if (xmax < 0) xmax = h2->GetNbinsX()+1;
  
  TH1 *h = (TH1*)h2->ProjectionY( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int ybin(0); ybin <= NbinsY+1; ybin++ ) {
    int nvals(0);
    double avg(0), err(0);
    for( int xbin = xmin; xbin <= xmax; xbin++ ) { 
      //avg += h2->GetBinContent( xbin, ybin ); 
      double val = h2->GetBinContent( xbin, ybin );
      double dx = h2->GetBinError( xbin, ybin );
      if( val <= 0 ) continue;
      nvals++;
      avg += val;
      err += (dx*dx);
    }
    if (nvals <= 0) continue;
    avg /= nvals;
    err = sqrt(err)/nvals;
    h->SetBinContent(ybin, avg);
    h->SetBinError(ybin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.035 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawSliceX( TH2 *h2, TString opt, int col=kBlack, int ymin=0, int ymax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceX(h2,ymin,ymax), opt, col, useMax);
}

TH1 *drawSliceY( TH2 *h2, TString opt, int col=kBlack, int xmin=0, int xmax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceY(h2,xmin,xmax), opt, col, useMax);
}

//////////////////////////  1D Onset Fit Function   //////////////////////////

double sqr(double a) {return a*a;}

double func_fixocc(double *x, double *par) {
  //par 12 is the occupancy.
  if (x[0] < 0.0) return 0.0;

  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  
  //give parameters occupancy dependence
  double occ = par[10];

  double par1 = par[1] + par[6]*occ;
  double par4 = par[4] + par[7]*occ;
  
  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par4)/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par1);
  
  double pHT_OccZero; 
  if (log10(x[0])      < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(x[0]) > par[1]) pHT_OccZero = pHT_HG;
  else                           pHT_OccZero = pHT_TR;

  double OccFit = par[2] + par[8]*occ + par[9]*sqr(occ);
  double pHT = pHT_OccZero + (1-pHT_OccZero)*(OccFit - par[2]);
  return pHT;
}

double func_fixgam(double *x, double *par) {
  //par 12 is the occupancy.
  if (x[0] < 0.0) return 0.0;

  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  
  //give parameters occupancy dependence
  double gam = par[10];
  double occ = x[0];

  double par1 = par[1] + par[6]*occ;
  double par4 = par[4] + par[7]*occ;
  
  // TR onset part (main part):
  double exp_term = exp(-(log10(gam) - par4)/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(gam) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(gam) - par1);
  
  double pHT_OccZero; 
  if (log10(gam)      < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(gam) > par[1]) pHT_OccZero = pHT_HG;
  else                           pHT_OccZero = pHT_TR;

  double OccFit = par[2] + par[8]*occ + par[9]*sqr(occ);
  double pHT = pHT_OccZero + (1-pHT_OccZero)*(OccFit - par[2]);
  return pHT;
}

//////////////////////////  Make 1D Functions of Fits   //////////////////////////

TF1 *getSliceOcc( TH1 *h, int iGas, int TRTpart, double occ ) {
  
  TString fname = TString::Format("f1_pHT_%d_%d", TRTpart, iGas);
  TF1 *fn = new TF1( fname, func_fixocc, 50, 1000000, 11 );
  for( int ipar(0); ipar < 10; ipar++ ) { fn->SetParameter( ipar, h->GetBinContent(ipar+1) ); }
  fn->FixParameter( 10, occ );

  return fn; 
}


TF1 *getSliceGam( TH1 *h, int iGas, int TRTpart, double gam ) {
  
  TString fname = TString::Format("f1_pHT_%d_%d", TRTpart, iGas);
  TF1 *fn = new TF1( fname, func_fixgam, 50, 1000000, 11 );
  for( int ipar(0); ipar < 10; ipar++ ) { fn->SetParameter( ipar, h->GetBinContent(ipar+1) ); }
  fn->FixParameter( 10, gam );

  return fn; 
}


//////////////////////////    2D Onset Fit Functions   //////////////////////////

double func_onset(double *x, double *par) {
  // get input variables
  double gamma = x[0];
  double occ = x[1];

  // give parameters occupancy dependence
  double par1 = par[1] + par[6]*occ;
  double par4 = par[4] + par[7]*occ;
  
  // TR onset part (main part):
  double exp_term = exp(-(log10(gamma) - par4)/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(gamma) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(gamma) - par1);
  
  double pHT_OccZero; 
  if (log10(gamma)      < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(gamma) > par1 )  pHT_OccZero = pHT_HG; 
  else                            pHT_OccZero = pHT_TR;

  double OccFactor = par[8]*occ + par[9]*sqr(occ);
  return pHT_OccZero + (1-pHT_OccZero)*(OccFactor);
}

TH2 *get2DpHT( TFile *fee, TFile *fmm, TString gas, TString part, double SystErr = 0.0 ) {
  // Collect all of the 2D histograms
  TString suffix = TString::Format( "_%s_%s_GF_OT", gas.Data(), part.Data() );

  TH2 *h_nLTee = getHisto2D( fee, "h2_nLT"+suffix );
  TH2 *h_nLTmm = getHisto2D( fmm, "h2_nLT"+suffix );
 
  TH2 *h_nHTee = getHisto2D( fee, "h2_nHTMB"+suffix );
  TH2 *h_nHTmm = getHisto2D( fmm, "h2_nHTMB"+suffix );

  // Make combined plots
  TH2 *h_pHT = (TH2*) h_nHTee->Clone();
  TH2 *h_nLT = (TH2*) h_nLTee->Clone();

  h_pHT->Add( h_nHTmm );
  h_nLT->Add( h_nLTmm );

  // Make ratio probability HT and return
  h_pHT->Divide( h_nLT );

  // Loop through every bin and calculate binomail errors
  for ( int xbin(0); xbin < h_nLT->GetNbinsX()+2; xbin++ ) { 
    for ( int ybin(0); ybin < h_nLT->GetNbinsY()+2; ybin++ ) { 
      double nLT = h_nLT->GetBinContent(xbin, ybin);
      if (nLT <= 0.) continue;
      
      double eff = h_pHT->GetBinContent(xbin, ybin);
      double error = eff*(1-eff) / nLT;
      error += (SystErr*SystErr);
      error = sqrt(error);
      
      h_pHT->SetBinError( xbin, ybin, error );
    }
  }

  return h_pHT;
}

TH1 *getOnsetFit( TFile *fee, TFile *fmm, TString gas, int ipart ) {
  // Define Variables
  double gamMin[3] = { 50.0, 75.0, 100.0};
  TString parts[3] = { "BRL", "ECA", "ECB" };
  
  // Get 2D histogram for fit
  TH2 *h2_onset = get2DpHT( fee, fmm, gas, parts[ipart], 0.0 ); // SystErr = 0.005
  h2_onset->Draw("COLz");

  // Define Fit Function 
  TString fname = TString::Format( "ONSET_%s_%s", gas.Data(), parts[ipart].Data() );
  TF2* fit = new TF2( fname, func_onset, gamMin[ipart], 1000000.0, 0.0, 0.75, 10);

  // Initialize Fit Parameters
  // Xenon 
  if ( gas.EqualTo("Xe") ) {
    if (ipart == 0) fit->SetParameters( 1.8, 3.8, 0.04, 0.12, 3.0, 0.22, 1.0, 0.1, 0.0, 0.0 );
    else            fit->SetParameters( 1.8, 3.7, 0.05, 0.14, 3.0, 0.18, 1.0, 0.1, 0.0, 0.0 );

  // Argon
  } else if ( gas.EqualTo("Ar") ) {
    if (ipart == 0) fit->SetParameters( 1.8, 3.5, 0.03, 0.04, 2.85, 0.22, 1.0, 0.1, 0.0, 0.0 );
    else            fit->SetParameters( 1.8, 3.6, 0.05, 0.08, 3.05, 0.18, 1.0, 0.1, 0.0, 0.0 );
  }

  // Constrain Fit Parameters
  fit->SetParLimits( 0, 1.0, 6. );
  fit->SetParLimits( 1, 1.0, 6. );
  fit->SetParLimits( 2, 0., 1. );
  fit->SetParLimits( 3, 0., 1. );
  fit->SetParLimits( 4, 1.0, 6. );
  fit->SetParLimits( 5, 0., 1. );
  fit->SetParLimits( 6, 0., 1. );
  fit->SetParLimits( 7, 0., 1. );
  fit->SetParLimits( 8, 0., 1. );
  fit->SetParLimits( 9, 0., 1. );
 
  // Fix Unused Parameters
  fit->FixParameter( 0, 1.0 );
  fit->FixParameter( 6, 0.0 ); 
  if (gas.EqualTo("Xe")) fit->FixParameter( 9, 0.0 ); // might have to release this for argon fits

  // Fit 2D Onset 
  TFitResultPtr r = h2_onset->Fit( fname, "R" );

  // Print Fit Parameters and Summary 
  printf("  %s:  %7.4f %7.4f   %7.4f %7.4f %7.4f %7.4f   %7.4f %7.4f   %7.4f %7.4f \n", 
   parts[ipart].Data(),
   fit->GetParameter(0),  fit->GetParameter(1),
   fit->GetParameter(2),  fit->GetParameter(3),
   fit->GetParameter(4),  fit->GetParameter(5),
   fit->GetParameter(6),  fit->GetParameter(7),
   fit->GetParameter(8),  fit->GetParameter(9) );

  printf("  Chi2: %6.1f  Ndof: %5d  Prob: %6.4f \n\n", fit->GetChisquare(), fit->GetNDF(), fit->GetProb());

  // Write parameters to histogram and return
  gas.ToUpper();
  TString hname = TString::Format( "ONSET_%s_%s", gas.Data(), parts[ipart].Data() );
  TH1F *pars = new TH1F( hname, hname, 10, 0, 10 );
  for( int ipar(0); ipar < 10; ipar++ )
    pars->SetBinContent( ipar+1, fit->GetParameter(ipar) );
  return pars;
}




////////////////////////// Correction Factor Functions //////////////////////////

TH1 *makeCalibHist( TFile *f, TString type, TString gas, TString part, TString var ) {
  
  // Get Average and Original TProfile
  TString prefix = TString::Format( "h_pHTMB_%s_%s_", gas.Data(), part.Data() );
  double avg = getHisto( f, prefix+"AVG" )->GetBinContent(1);
  TH1 *h = (TH1*)getHisto( f, prefix+var );
  h->Scale( 1.0/avg );

  // Need to create a new TH1F, since TProfile -> TH1 conversion is strange
  gas.ToUpper();
  TString hcName = TString::Format( "CALIB_%s_%s_%s_%s", gas.Data(), type.Data(), part.Data(), var.Data() );
  TH1F *hc = new TH1F( hcName, hcName, h->GetNbinsX(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax() );

  bool useDebug(false);
  if (useDebug) {
    cout << endl;
    cout << hcName << endl;
    cout << "-------------------------" << endl;
  }
 
  // Fill Bins of Histogram
  for( int ibin(1); ibin < hc->GetNbinsX()+1; ibin++ ) {
    hc->SetBinContent( ibin, h->GetBinContent(ibin) );
    
    // Don't trust large errors > 4.5 % 
    double error = h->GetBinError(ibin) / h->GetBinContent(ibin);
    /*
    if (error > 0.045 ) { 
      if (useDebug) 
        cout << "Bin " << ibin << "  " << var << " = " << h->GetBinCenter(ibin) << "  Error = " << error << endl;
      hc->SetBinContent( ibin, 1.0 );
    }
    */
   
    // Fill Gaps
    //if( hc->GetBinContent(ibin) <= 0.0 ) hc->SetBinContent(ibin, 0, 1.0);
    
    // Fill gaps and TW overflow
    /*
    if ( ( hc->GetBinContent(ibin) <= 0.0)  || ( var.EqualTo("TW") && hc->GetBinLowEdge(ibin) >= 2.0 ) ) { 
      hc->SetBinContent(ibin, 0, 1.0);
    }
    */

  }
  
  // Prepare Title and Name for Writing
  hc->SetName(hcName);
  hc->SetTitle(hcName);
  return hc;
}




////////////////// make calibration histograms //////////////////////

void makeCalibFile() {

  // Get Input Files 
  // -----------------------------------------------------------
  TFile *fee = openFile("hists_PID_01_07/data-Zee.root");
  TFile *fmm = openFile("hists_PID_01_07/data-Zmumu.root");


  // Create output File
  // -----------------------------------------------------------
  TFile *out = new TFile("calib_data15.root","RECREATE");
  out->cd();

 
  // Get 2D Onset Parameters and write to file
  // -----------------------------------------------------------
  TString gases[2] = {"Xe","Ar"};
  TString parts[3] = {"BRL","ECA","ECB"};
  TH1 *fitpars[2][3];

  for( int igas(0); igas < 2; igas++ ) { 
    for( int ipart(0); ipart < 3; ipart++ ) { 
      //if( gases[igas].EqualTo("Ar") && ipart==3 ) continue;
      //printf("\n\n %s - %s \n\n",parts[ipart].Data(),gases[igas].Data() );
      TH1 *pars = getOnsetFit( fee, fmm, gases[igas], ipart );
      fitpars[igas][ipart] = (TH1*) pars->Clone();
      pars->Write();
    }
  }


  // Make 2D Onset Plots from Fits
  // -----------------------------------------------------------


  // Draw Onset Slices for Validation
  // -----------------------------------------------------------
  TCanvas *can = new TCanvas("can","can");
  can->cd();
  can->SetLogx();

  int cols[6] = {kBlack, kRed, kGreen+2, kBlue, kViolet};
  TH1 *h_data[6], *h_fit[6];

  can->Print("test.pdf[");
  for( int igas(0); igas < 2; igas++ ) {
    for( int ipart(0); ipart < 3; ipart++ ) {
      TString gas = gases[igas];
      TString part = parts[ipart];
      //if ( gas.EqualTo("Ar") && part.EqualTo("ECB") ) continue;

      // Draw Slices in Occupancy
      TH2 *h2_data = get2DpHT( fee, fmm, gas, part ); 
      for( int i=1; i < 4; i++ ) {
        TString opt = (i>1) ? "PE SAME" : "PE";
        TH1 *htempMC  = drawSliceX( h2_data, opt, cols[i], 1+i*5, 5+i*5, 0.45);
        h_data[i] = (TH1*) htempMC->Clone();
        h_data[i]->SetTitle( TString::Format( "ONSET_%s_%s", gas.Data(), parts[ipart].Data() ) );
        h_data[i]->SetMarkerStyle(2);
        h_data[i]->SetMarkerColor(cols[i]);
        h_data[i]->Draw( opt );
        
        TF1 *fit = getSliceOcc( fitpars[igas][ipart], igas, ipart, 0.05 + 0.1*i ); 
        fit->SetLineColor( cols[i] );
        fit->SetLineWidth( 1 );
        fit->SetLineStyle( 2 );
        fit->Draw("SAME");
      }
      can->Print("test.pdf");
     
      // Draw 2D Hist from Fit for comparison
      /*TH2 *h2_fit = (TH2*)h2_data->Clone();
      for( int i(1), nx(h2_fit->GetNbinsX()); i <= nx; i++) {
        for( int j(1), ny(h2_fit->GetNbinsY()); j <= ny; j++) {
          double gamma = h2_fit->GetXaxis()->GetBinCenter(i);
          double occ   = h2_fit->GetYaxis()->GetBinCenter(j);
          double pHT = 0; //func_onset( {gamma,occ} ,fitpars[igas][ipart] ); 
          if ( h2_fit->GetBinContent(i,j) == 0 ) continue;
          h2_fit->SetBinContent( i, j, pHT );
          h2_fit->SetBinError( i, j, 0.0 );
        }
      }*/
      // Draw Slices in Gamma 
    
    }
  }
  can->Print("test.pdf]");


  // Create Calbiration Histograms 
  // -----------------------------------------------------------
  for( TString gas: {"Xe","Ar"} ) {
  
    // Prepare and Write CFs for Electrons 
    // ---------------------------------------------------------
    for( TString part: {"BRL","ECA","ECB"} ) {
      for( TString var: {"SL","TW","ZR"} ) { 
        TH1 *hee = makeCalibHist( fee, "EL", gas, part, var );
        hee->Write();
      }
    }

    // Prepare and Write CFs for Muons 
    // ---------------------------------------------------------
    for( TString part: {"BRL","ECA","ECB"} ) {
      for( TString var: {"SL","TW","ZR"} ) { 
        TH1 *hmm = makeCalibHist( fmm, "MU", gas, part, var );
        hmm->Write();
      }
    }

  }


  // Close files
  // -----------------------------------------------------------
  fee->Close();
  fmm->Close();
  out->Close();

}
